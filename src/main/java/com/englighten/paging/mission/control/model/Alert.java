package com.englighten.paging.mission.control.model;

import java.util.Date;

public class Alert {

	private Integer _satelliteId;
	private String _severity;
	private String _component;
	private Date _timestamp;
	
	public Alert(Integer _satelliteId, String _severity, String _component, Date _timestamp) {
		super();
		this._satelliteId = _satelliteId;
		this._severity = _severity;
		this._component = _component;
		this._timestamp = _timestamp;
	}

	public Integer getSatelliteId() {
		return _satelliteId;
	}

	public String getSeverity() {
		return _severity;
	}

	public String getComponent() {
		return _component;
	}

	public Date getTimestamp() {
		return _timestamp;
	}

	
	
	
	
	
}
