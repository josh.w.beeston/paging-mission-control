package com.englighten.paging.mission.control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.englighten.paging.mission.control.model.Alert;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class Problem {

	private static final Integer TIMESTAMP_INDEX = 0;
	private static final Integer SAT_ID_INDEX = 1;
	private static final Integer THERM_HIGH_INDEX = 2;
	private static final Integer BAT_LOW_INDEX = 5;
	private static final Integer VALUE_INDEX = 6;
	private static final Integer COMPONENT_INDEX = 7;
	
	private static final String BATTERY = "BATT";
	private static final String TEMPERATURE = "TSTAT";
	
	private static final String RED_LOW = "RED LOW";
	private static final String RED_HIGH = "RED HIGH";
	
	private static final SimpleDateFormat _timestampFormat = 
			new SimpleDateFormat("yyyyMMdd HH:mm:ss.S");	
	private static final SimpleDateFormat _alertTimestampFormat =  
			new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
	
	public void generateAlerts(File file) throws Exception {
	
		List<Alert> alerts = new ArrayList<>();
		Map<String, Map<Integer, List<Date>>> readings = new HashMap<>();
		readings.put(BATTERY, new HashMap<>());
		readings.put(TEMPERATURE, new HashMap<>());
		
		BufferedReader reader;

		try {
			
			reader = new BufferedReader(new FileReader(file));
			String teleData = reader.readLine();

			while (teleData != null) {
				
				String[] arr = teleData.split("[|]");
				
				//Get telemetry data
				Date timestamp = _timestampFormat.parse(arr[TIMESTAMP_INDEX]);
				int satId = Integer.parseInt(arr[SAT_ID_INDEX]);
				int thermHigh = Integer.parseInt(arr[THERM_HIGH_INDEX]);
				int batLow = Integer.parseInt(arr[BAT_LOW_INDEX]);
				double value = Double.parseDouble(arr[VALUE_INDEX]);
				String component = arr[COMPONENT_INDEX];
				
				//Only do anything if meets criteria
				if((component.equals(BATTERY) && value < batLow) || (component.contentEquals(TEMPERATURE) && value > thermHigh)) {
					
					//Get the timestamps for specified readings for this satellite
					//over the last 5 minutes
					Map<Integer, List<Date>> satToTimestamps = readings.get(component);
					satToTimestamps.putIfAbsent(satId, new ArrayList<>());
					List<Date> currReadingTimes = satToTimestamps.get(satId);
					currReadingTimes.add(timestamp);
					List<Date> validReadingTimes = _getReadingTimes(timestamp, currReadingTimes);
					
					//If 3 timestamps, build an alert message
					if(validReadingTimes.size() == 3) {
						alerts.add(
								new Alert(satId, component.equals(BATTERY) ? RED_LOW : RED_HIGH, component, validReadingTimes.get(0)));
					}
					
					satToTimestamps.put(satId, validReadingTimes);
					readings.put(component, satToTimestamps);
				}
			
				teleData = reader.readLine();
			}
			
			reader.close();
			
			_buildAndPrintJson(alerts);
		} 
		
		//don't do anything if error thrown
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<Date> _getReadingTimes(Date currTime, List<Date> timeStamps) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(currTime);
		cal.add(Calendar.MINUTE, -5);
		
		Date fiveMinutesBack = cal.getTime();
		
		return timeStamps
				.stream()
				.filter(prevReadingTime -> prevReadingTime.after(fiveMinutesBack))
				.collect(Collectors.toList());
	}
	
	private JsonObject _buildAlertJson(Alert alert) {
		JsonObject alertJson = new JsonObject();
		
		alertJson.addProperty("satelliteId", alert.getSatelliteId());
		alertJson.addProperty("severity", alert.getSeverity());
		alertJson.addProperty("component", alert.getComponent());
		alertJson.addProperty("timestamp", _alertTimestampFormat.format(alert.getTimestamp()));
		
		return alertJson;
	}
	
	private void _buildAndPrintJson(List<Alert> alerts) {
		
		//no output if no alerts
		if(alerts.isEmpty()) {
			return;
		}
		
		//build json string
		GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
		Gson gson = gb.create();
		
		JsonArray alertArr = new JsonArray();
		for(Alert alert : alerts) {
			alertArr.add(_buildAlertJson(alert));
		}
		
		System.out.println(gson.toJson(alertArr));
	}
	
}
