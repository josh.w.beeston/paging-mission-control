package com.englighten.paging.mission.control;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestProblem {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	}
	
	@After
	public void restoreStreams() {
	    System.setOut(originalOut);
	}

	Problem problem = new Problem();

	
	@Test
	public void testSampleOutput() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("sample_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();
		
		assertEquals(2, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(2, StringUtils.countMatches(output, "1000"));
		assertEquals(2, StringUtils.countMatches(output, "severity"));
		assertEquals(1, StringUtils.countMatches(output, "RED HIGH"));
		assertEquals(1, StringUtils.countMatches(output, "RED LOW"));
		assertEquals(2, StringUtils.countMatches(output, "component"));
		assertEquals(1, StringUtils.countMatches(output, "TSTAT"));
		assertEquals(1, StringUtils.countMatches(output, "BATT"));
		assertEquals(2, StringUtils.countMatches(output, "timestamp"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:38.001Z"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:09.521Z"));
	}
	
	@Test
	public void testOnlyTwo() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("only_2_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();
		
		assertEquals(0, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(0, StringUtils.countMatches(output, "severity"));
		assertEquals(0, StringUtils.countMatches(output, "component"));
		assertEquals(0, StringUtils.countMatches(output, "timestamp"));
	}
	
	@Test
	public void testOnlyThreeTemp() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("only_3_temp_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();
		
		assertEquals(1, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(1, StringUtils.countMatches(output, "1000"));
		assertEquals(1, StringUtils.countMatches(output, "severity"));
		assertEquals(1, StringUtils.countMatches(output, "RED HIGH"));
		assertEquals(1, StringUtils.countMatches(output, "component"));
		assertEquals(1, StringUtils.countMatches(output, "TSTAT"));
		assertEquals(1, StringUtils.countMatches(output, "timestamp"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:38.001Z"));
	}
	
	@Test
	public void testOnlyThreeBatt() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("only_3_batt_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();

		assertEquals(1, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(1, StringUtils.countMatches(output, "1000"));
		assertEquals(1, StringUtils.countMatches(output, "severity"));
		assertEquals(1, StringUtils.countMatches(output, "RED LOW"));
		assertEquals(1, StringUtils.countMatches(output, "component"));
		assertEquals(1, StringUtils.countMatches(output, "BATT"));
		assertEquals(1, StringUtils.countMatches(output, "timestamp"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:38.001Z"));
	}
	
	@Test
	public void testOnlyThreeEach() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("only_3_each_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();
		
		assertEquals(2, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(2, StringUtils.countMatches(output, "severity"));
		assertEquals(2, StringUtils.countMatches(output, "component"));
		assertEquals(2, StringUtils.countMatches(output, "timestamp"));
		
		assertEquals(2, StringUtils.countMatches(output, "1000"));
		assertEquals(1, StringUtils.countMatches(output, "RED HIGH"));
		assertEquals(1, StringUtils.countMatches(output, "TSTAT"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:38.001Z"));
		assertEquals(1, StringUtils.countMatches(output, "RED LOW"));
		assertEquals(1, StringUtils.countMatches(output, "BATT"));
		assertEquals(1, StringUtils.countMatches(output, "2018-01-01T23:01:38.002Z"));
	}
	
	@Test
	public void testOnlyThreeOutOfTimerange() throws Exception {
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("only_3_out_of_timerange_output.txt").getFile());
		
		problem.generateAlerts(file);
		
		String output = outContent.toString();
		
		assertEquals(0, StringUtils.countMatches(output, "satelliteId"));
		assertEquals(0, StringUtils.countMatches(output, "severity"));
		assertEquals(0, StringUtils.countMatches(output, "component"));
		assertEquals(0, StringUtils.countMatches(output, "timestamp"));
	}
	
	
}
